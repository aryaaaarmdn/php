<?php

require_once "../_config/config.php";

if (isset($_SESSION['user'])) {
    // header('Location: ' . base_url());
    echo "<script>
            window.location='" . base_url() . "';
        </script>";
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login - Rumah Sakit</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Style -->
    <style>
        #wrapper {
            height: 100vh;
            display: flex;
            align-items: center;
        }
    </style>

</head>

<body>

    <div id="wrapper">
        <div class="container">
            <center>
                <?php
                if (isset($_POST['login'])) {
                    $user = trim(mysqli_real_escape_string($koneksi, $_POST['user']));
                    $password = sha1(trim(mysqli_real_escape_string($koneksi, $_POST['password'])));

                    $query = "SELECT * FROM user WHERE username = '$user' AND password = '$password'";
                    $sql_login = mysqli_query($koneksi, $query) or die(mysqli_error($koneksi));

                    if (mysqli_num_rows($sql_login) > 0) {
                        $_SESSION['user'] = $user;
                        echo "<script>
                                window.location='" . base_url() . "';
                            </script>";
                    } else { ?>
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3">
                                <div class="alert alert-danger alert-dismissable" role="alert">
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <strong>Login Gagal</strong> Username / Password salah
                                </div>
                            </div>
                        </div>
                <?php

                    }
                }
                ?>

                <form action="" method="post" class="navbar-form">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-user"></i>
                        </span>
                        <input type="text" name="user" class="form-control" required placeholder="Username.." autofocus>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-lock"></i>
                        </span>
                        <input type="password" name="password" class="form-control" required placeholder="Password..">
                    </div>
                    <div class="input-group">
                        <button type="submit" name="login" class="btn btn-primary">Login</button>
                    </div>
                </form>
            </center>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?= base_url(); ?>/assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url(); ?>/assets/js/bootstrap.min.js"></script>

</body>

</html>