<?php

date_default_timezone_set('Asia/Jakarta');
session_start();

$koneksi = mysqli_connect('localhost', 'root', '', 'rumahsakit');

if (!$koneksi) {
    echo mysqli_connect_error();
}

function base_url($url = null)
{
    $base_url = 'http://localhost/PHP/Rekam-Medis';

    if ($url != null) {
        return $base_url . '/' . $url;
    } else {
        return $base_url;
    }
}
