<?php

session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

require 'functions.php';

// ambil id dari url
$id = $_GET["id"];

// query data berdasarkan id
$mhs = query("SELECT * FROM mahasiswa where id = $id")[0];

// cek apakah data berhasl ditambahkan atau tidak

if (isset($_POST["submit"])) {

    if (ubah($_POST) > 0) {
        echo "
                <script>
                    alert('Data Berhasil Diubah');
                    document.location.href = 'index.php';
                </script>
            ";
    } else {
        echo "
                <script>
                    alert('Data Gagal Diubah');
                    document.location.href = 'index.php';
                </script>
        ";
    }
}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Data</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-min.css">
</head>

<body>

    <!-- As a heading -->
    <nav class="navbar navbar-light bg-dark">
        <div class="container">
            <span class="navbar-brand mb-0 h1 text-primary">Edit Data Mahasiswa</span>
        </div>
    </nav>

    <div class="container mt-5">
        <table class="table">
            <form action="" method="post" enctype="multipart/form-data">
                <thead>
                    <tr>
                        <input type="hidden" name="id" value="<?= $mhs["id"]; ?>">
                        <input type="hidden" name="gambarLama" value="<?= $mhs["gambar"]; ?>">
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <th>
                            <label for="nim">NIM :</label>
                        </th>
                        <th>
                            <input type="text" name="nim" id="nim" value="<?= $mhs["nim"]; ?>">
                        </th>
                    </tr>

                    <tr>
                        <th>
                            <label for="nama">Nama :</label>
                        </th>
                        <th>
                            <input type="text" name="nama" id="nama" value="<?= $mhs["nama"]; ?>">
                        </th>
                    </tr>

                    <tr>
                        <th>
                            <label for="email">Email :</label>
                        </th>

                        <th>
                            <input type="text" name="email" id="email" value="<?= $mhs["email"]; ?>">
                        </th>
                    </tr>

                    <tr>
                        <th>
                            <label for="jurusan">Jurusan :</label>
                        </th>

                        <th>
                            <input type="text" name="jurusan" id="jurusan" value="<?= $mhs["jurusan"]; ?>">
                        </th>
                    </tr>

                    <tr>
                        <th>
                            <label for="gambar">Gambar :</label><br>
                        </th>

                        <th>
                            <img src="img/<?= $mhs['gambar']; ?>" width="70">
                        </th>
                    </tr>

                    <tr>
                        <th></th>
                        <th>
                            <input type="file" name="gambar" id="gambar">
                        </th>
                    </tr>

                    <tr>
                        <th>
                            <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                        </th>
                    </tr>
                </tbody>
            </form>
        </table>
    </div>

    <script src="bootstrap/js/bootstrap.min.js"></script>


</body>

</html>