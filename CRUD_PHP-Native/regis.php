<?php

require 'functions.php';

if (isset($_POST["register"])) {

    // > 0 karena mengambil nilai dari mysqli_affected_rows
    if (registrasi($_POST) > 0) {
        echo "<script>
                    alert ('Registrasi Berhasil');
                    window.location.href = 'login.php';
                </script>";
    } else {
        echo mysqli_error($conn);
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Halaman Registrasi</title>
    <style>
        label {
            display: block;
        }
    </style>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-min.css">

</head>

<body>

    <!-- As a heading -->
    <nav class="navbar navbar-light bg-dark">
        <div class="container">
            <span class="navbar-brand mb-0 h1 text-primary">Halaman Registrasi</span>
        </div>
    </nav>

    <div class="container mt-5">
        <form action="" method="post">
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" aria-describedby="emailHelp" name="username">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="form-group">
                <label for="password2">Konfirmasi Pasword</label>
                <input type="password" class="form-control" id="password2" name="password2">
            </div>
            <button type="submit" class="btn btn-primary" name="register">Daftar</button>
        </form>





        <!-- <form action="" method="post">
            <ul>
                <li>
                    <label for="username">Username : </label>
                    <input type="text" name="username" id="username">
                </li>

                <li>
                    <label for="password">Password :</label>
                    <input type="password" name="password" id="password">
                </li>

                <li>
                    <label for="password2">Konfirmasi Password : </label>
                    <input type="password" name="password2" id="password2">
                </li>

                <li>
                    <button type="submit" name="register">Daftar !!</button>
                </li>
            </ul>
        </form> -->

        <script src="bootstrap/js/bootstrap.min.js"></script>

</body>

</html>