<?php

session_start();

if ( !isset($_SESSION["login"]) ){
    header("Location: login.php");
    exit;
}

require'functions.php';

// Menangkap id dari URL
$id = $_GET["id"];
$gambar = $_GET["gambar"];

if ( hapus($id , $gambar) > 0 ){

    echo "
            <script>
                alert('Data Berhasil DiHapus');
                document.location.href = 'index.php';
            </script>
        ";
} else {
    echo "
            <script>
                alert('Data Gagal DiHapus');
                document.location.href = 'index.php';
            </script>
    ";
}

?>