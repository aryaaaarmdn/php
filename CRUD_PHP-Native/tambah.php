<?php

session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

require 'functions.php';

// cek apakah data berhasl ditambahkan atau tidak

if (isset($_POST["submit"])) {

    if (tambah($_POST) > 0) {
        echo "
                <script>
                    alert('Data Berhasil Ditambah');
                    document.location.href = 'index.php';
                </script>
            ";
    } else {
        echo "
                <script>
                    alert('Data Gagal Ditambah');
                    document.location.href = 'index.php';
                </script>
        ";
    }
}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tambah Data</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-min.css">

</head>

<body>

    <!-- As a heading -->
    <nav class="navbar navbar-light bg-dark">
        <div class="container">
            <span class="navbar-brand mb-0 h1 text-primary">Tambah Data Mahasiswa</span>
        </div>
    </nav>

    <div class="container mt-5">
        <table class="table">
            <form action="" method="post" enctype="multipart/form-data">
                <thead>
                    <tr>
                        <th>
                            <label for="">Silahkan Isi Data</label>
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>
                            <label for="nim">NIM </label>
                        </th>
                        <th>
                            <input type="text" name="nim" id="nim">
                        </th>
                    </tr>

                    <tr>
                        <th>
                            <label for="nama">Nama </label>
                        </th>
                        <th>
                            <input type="text" name="nama" id="nama">
                        </th>
                    </tr>

                    <tr>
                        <th>
                            <label for="email">Email </label>
                        </th>
                        <th>
                            <input type="text" name="email" id="email">
                        </th>
                    </tr>

                    <tr>
                        <th>
                            <label for="jurusan">Jurusan </label>
                        </th>
                        <th>
                            <input type="text" name="jurusan" id="jurusan">
                        </th>
                    </tr>

                    <tr>
                        <th>
                            <label for="nim">Gambar </label>
                        </th>
                        <th>
                            <input type="file" name="gambar" id="gambar">
                        </th>
                    </tr>

                    <tr>
                        <th>
                            <button type="submit" name="submit" class="btn btn-info">Tambah</button>
                        </th>
                    </tr>
                </tbody>
            </form>
        </table>
    </div>

    <script src="bootstrap/js/bootstrap.min.js"></script>

</body>

</html>