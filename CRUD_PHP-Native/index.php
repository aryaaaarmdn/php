<?php

session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

require 'functions.php'; // menyisipkan file require.php ke dalam file ini

$mahasiswa = query("SELECT * FROM mahasiswa"); // ambil data dari tabel mahasiswa (Ambil Lemari)

// tombol cari di tekan
if (isset($_POST["cari"])) {
    $mahasiswa = cari($_POST["keyword"]);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Halaman Admin</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-min.css">

</head>

<body>

    <!-- As a heading -->
    <nav class="navbar navbar-light bg-dark">
        <div class="container">
            <a class="navbar-brand text-primary" href="index.php">Daftar Mahasiswa</a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li>
                <button type="button" class="btn btn-light">
                    <a href="logout.php">Logout</a>
                </button>
            </li>
        </ul>
    </nav>

    <div class="container mt-4 mb-4">
        <button type="button" class="btn btn-info">
            <a href="tambah.php" class="text-secondary">Tambah Data Mahasiswa</a>
        </button>
    </div>


    <div class="container mb-4 text-center">
        <form action="" method="post">
            <input type="text" name="keyword" autofocus placeholder="Masukkan Keyword Yang Ingin Dicari" size="50" autocomplete="off">
            <button type="submit" name="cari">Telusuri</button>
        </form>
    </div>


    <div class="container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Aksi</th>
                    <th scope="col">Gambar</th>
                    <th scope="col">NIM</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Jurusan</th>
                </tr>
            </thead>

            <?php $i = 1; ?>
            <?php foreach ($mahasiswa as $row) :  ?>

                <tbody>
                    <tr>
                        <th scope="row"><?= $i; ?></th>
                        <td>
                            <a href="ubah.php?id=<?= $row["id"]; ?>">Ubah</a> |
                            <a href="hapus.php?id=<?= $row["id"]; ?>&gambar=<?= $row["gambar"]; ?>" onclick="return confirm('Yakin ?');">Hapus</a>
                        </td>
                        <td>
                            <img src="img/<?= $row['gambar']; ?>" alt="">
                        </td>
                        <td>
                            <?= $row['nim']; ?>
                        </td>
                        <td>
                            <?= $row['nama']; ?>
                        </td>
                        <td>
                            <?= $row['email']; ?>
                        </td>
                        <td>
                            <?= $row['jurusan']; ?>
                        </td>
                    </tr>
                </tbody>
                <?php $i++; ?>
            <?php endforeach; ?>
        </table>
    </div>

    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

</body>

</html>