$(function () {

	$('.tombolTambahData').on('click', function () {
		$('.modal-body form').attr('id', 'tambahData');
		$('.modal-body form').removeAttr('action');
		$('#formModalLabel').html('Tambah Data Mahasiswa');
		$('.modal-footer #simpanData').html('Tambah Data');

		$('#id').val('');
		$('#nama').val('');
		$('#nim').val('');
		$('#email').val('');
		$('#jurusan').val('');

		$("#simpanData").click(function () {
			$.post('http://localhost/PHP/CI-APP/mahasiswa/add', $('#tambahData').serialize(),
				function (data) {
					if (data.code === 1) {
						// menampilkan semua error
						// $("#pesan").removeClass('alert-success').addClass('alert-danger').removeClass('hidden').html(data.msg);

						// menampilkan error satusatu (per field)
						$('#errorNama').html(data.nama);
						$('#errorNim').html(data.nim);
						$('#errorEmail').html(data.email);
						$('#errorJurusan').html(data.jurusan);
					} else {
						// location.href = 'mahasiswa'; // cukup nama controller saja yang di href kan
						location.href = 'http://localhost/PHP/CI-APP/mahasiswa';
					}
				}, 'json');
		});
	});

	$('.tampilModalUbah').on('click', function () {
		$('.modal-body form').attr('id', 'ubahData');
		$('#formModalLabel').html('Ubah Data Mahasiswa');
		$('.modal-footer #simpanData').html('Ubah Data');
		$('.modal-body form').removeAttr('action');

		const id = $(this).data('id');

		$.ajax({
			url: 'http://localhost/PHP/CI-APP/mahasiswa/getUbah',
			data: {
				id: id
			},
			method: 'post',
			dataType: 'json',
			success: function (data) {
				$('#id').val(data.id);
				$('#nama').val(data.nama);
				$('#nim').val(data.nim);
				$('#email').val(data.email);
				$('#jurusan').val(data.jurusan);
			}
		});

		$('.modal-footer #simpanData').click(function () {

			$.post('http://localhost/PHP/CI-APP/mahasiswa/ubah', $('#ubahData').serialize(),
				function (data) {
					if (data.code === 1) {
						// menampilkan error satusatu (per field)
						$('#errorNama').html(data.nama);
						$('#errorNim').html(data.nim);
						$('#errorEmail').html(data.email);
						$('#errorJurusan').html(data.jurusan);
					} else {
						// location.href = 'mahasiswa'; // cukup nama controller saja yang di href kan
						location.href = 'http://localhost/PHP/CI-APP/mahasiswa';
					}
				}, 'json');
		});
	})

});
