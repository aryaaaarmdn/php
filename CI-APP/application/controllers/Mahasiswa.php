<?php

class Mahasiswa extends CI_Controller
{
    public function __construct()
    {
        Parent::__construct();
        $this->load->model('Mahasiswa_model'); // load model agar bisa di pakai di setiap method pada controller mahasiswa
        $this->load->library('form_validation'); // load library validasi pada form
        $this->load->helper('form');
    }

    public function index()
    {
        // data yang akan di kirimkan ke view
        $data['judul'] = "Data Mahasiswa";
        $data['mahasiswa'] = $this->Mahasiswa_model->getAllMahasiswa();

        // Cari Data Mahasiswa
        if( $this->input->post('keyword') ) {
            $data['mahasiswa'] = $this->Mahasiswa_model->cariDataMahasiswa();
        }

        $this->load->view('templates/header', $data);
        $this->load->view('mahasiswa/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        // data yang akan dikirimkan ke view
        $data['judul'] = "Tambah Data Mahasiswa";

        // rules validasi
        $this->form_validation->set_rules('nama', 'nama', 'required|min_length[3]|regex_match[/^([a-z ])+$/i]');
        $this->form_validation->set_rules('nim', 'NIM', 'required|numeric|exact_length[10]|is_unique[mahasiswa.nim]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[mahasiswa.email]');
        $this->form_validation->set_rules('jurusan', 'jurusan', 'required|in_list[Teknik Informatika,Teknik Mesin,Teknik Elektro]');


        // Validasi
        if ( $this->form_validation->run() == false ) {
            $this->load->view('templates/header', $data);
            $this->load->view('mahasiswa/tambah');
            $this->load->view('templates/footer');
        } else {
            // echo 'ok';
            $this->session->set_flashdata('flash', 'ditambahkan');
            $this->Mahasiswa_model->tambahDataMahasiswa();
            redirect('mahasiswa'); // redirect ke controller mahasiswa method default (index)
        }

    }

    // tambah data menggunakan Jquery
    public function add() {

        // rules validasi
        $this->form_validation->set_rules('nama', 'nama', 'required|min_length[3]|regex_match[/^([a-z ])+$/i]');
        $this->form_validation->set_rules('nim', 'NIM', 'required|numeric|exact_length[10]|is_unique[mahasiswa.nim]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[mahasiswa.email]');
        $this->form_validation->set_rules('jurusan', 'jurusan', 'required|in_list[Teknik Informatika,Teknik Mesin,Teknik Elektro]');

        // Menampilkan Semua error
        // $data = array();
        // if ($this->form_validation->run() == FALSE) {
        //     $data = array(
        //         'code' => 1,
        //         'msg' => validation_errors()
        //     );
        // } else {
        //     $this->session->set_flashdata('flash', 'ditambahkan');
        //     $this->Mahasiswa_model->tambahDataMahasiswa();
        // }

        // menampilkan error satusatu (per field)
        $data = array();
        if ($this->form_validation->run() == FALSE) {

            $data['code'] = 1;

            if ( validation_errors() ) {
                $data['nama'] = form_error('nama');
                $data['nim'] = form_error('nim');
                $data['email'] = form_error('email');
                $data['jurusan'] = form_error('jurusan');
            }
        } else {
            $this->session->set_flashdata('flash', 'ditambahkan');
            $this->Mahasiswa_model->tambahDataMahasiswa();
        }

        echo json_encode($data);
    }


    public function hapus($id)
    {
        $this->Mahasiswa_model->hapusDataMahasiswa($id);
        $this->session->set_flashdata('flash', 'dihapus');
        redirect('mahasiswa');
    }

    public function detail($id)
    {
        $data['judul'] = "Detail Mahasiswa";

        $data['mahasiswa'] = $this->Mahasiswa_model->getMahasiswaByID($id); 

        $this->load->view('templates/header', $data);
        $this->load->view('mahasiswa/detail', $data);
        $this->load->view('templates/footer');
    }

    // ajax ambil data untuk ubah data
    public function getUbah() 
    {
        echo json_encode($this->Mahasiswa_model->getMahasiswaByID($_POST['id']));
    }

    // public function ubah($id)
    // {
    //     // data yang akan dikirimkan ke view
    //     $data['judul'] = "Ubah Data Mahasiswa";
    //     $data['mahasiswa'] = $this->Mahasiswa_model->getMahasiswaByID($id);
    //     $data['jurusan'] = ['Teknik Informatika', 'Teknik Mesin', 'Teknik Elektro'];

    //     // rules validasi
    //     $this->form_validation->set_rules('nama', 'nama', 'required|min_length[3]|regex_match[/^([a-z ])+$/i]');

    //     // rules nim
    //     if ( $this->input->post('nim') == $data['mahasiswa']['nim'] ) {
    //         $this->form_validation->set_rules('nim', 'NIM', 'required|numeric|exact_length[10]');    
    //     } else {
    //         $this->form_validation->set_rules('nim', 'NIM', 'required|numeric|exact_length[10]|is_unique[mahasiswa.nim]');
    //     }

    //     // rules email
    //     if ( $this->input->post('email') == $data['mahasiswa']['email'] ) {
    //         $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
    //     } else {
    //         $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[mahasiswa.email]');
    //     }

    //     $this->form_validation->set_rules('jurusan', 'jurusan', 'required|in_list[Teknik Informatika,Teknik Mesin,Teknik Elektro]');


    //     // Validasi
    //     if ( $this->form_validation->run() == false ) {
    //         $this->load->view('templates/header', $data);
    //         $this->load->view('mahasiswa/ubah', $data);
    //         $this->load->view('templates/footer');
    //     } else {
    //         // echo 'ok';
    //         $this->session->set_flashdata('flash', 'diubah');
    //         $this->Mahasiswa_model->ubahDataMahasiswa();
    //         redirect('mahasiswa'); // redirect ke controller mahasiswa method default (index)
    //     }

    // }

    public function ubah() 
    {

        $info['mahasiswa'] = $this->Mahasiswa_model->getMahasiswaByID($_POST['id']);

        // rules validasi
        $this->form_validation->set_rules('nama', 'nama', 'required|min_length[3]|regex_match[/^([a-z ])+$/i]');

        // rules nim
        if ( $this->input->post('nim') == $info['mahasiswa']['nim'] ) {
            $this->form_validation->set_rules('nim', 'NIM', 'required|numeric|exact_length[10]');    
        } else {
            $this->form_validation->set_rules('nim', 'NIM', 'required|numeric|exact_length[10]|is_unique[mahasiswa.nim]');
        }

        // rules email
        if ( $this->input->post('email') == $info['mahasiswa']['email'] ) {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        } else {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[mahasiswa.email]');
        }

        // menampilkan error satusatu (per field)
        $data = array();
        if ($this->form_validation->run() == FALSE) {

            $data['code'] = 1;

            if ( validation_errors() ) {
                $data['nama'] = form_error('nama');
                $data['nim'] = form_error('nim');
                $data['email'] = form_error('email');
                $data['jurusan'] = form_error('jurusan');
            }
        } else {
            $this->session->set_flashdata('flash', 'diubah');
            $this->Mahasiswa_model->ubahDataMahasiswa();
        }
        
        echo json_encode($data);
    }
}