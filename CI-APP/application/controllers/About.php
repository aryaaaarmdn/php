<?php

class About extends CI_Controller
{
    public function index()
    {
        $data['judul'] = "Tentang Saya";
        $data['nama'] = "Aryaa Ramadhan";
        $data['status'] = "Mahasiswa";

        $this->load->view('templates/header', $data);
        $this->load->view('about/index', $data);
        $this->load->view('templates/footer');
    }
}