<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Aryaaaarmdn <?= date('Y'); ?></span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?= base_url('auth/logout'); ?>">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/vendor/jquery/jquery.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/js/sb-admin-2.min.js'); ?>"></script>

<script>
    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();

        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });

    $('.form-check-input').on('click', function() {
        const menu_id = $(this).data('menu');
        const role_id = $(this).data('role');


        $.ajax({
            url: "<?= base_url('admin/changeAccess'); ?>",
            type: 'post',
            data: {
                menuID: menu_id,
                role_id: role_id
            },
            success: function() {
                document.location.href = "<?= base_url('admin/roleAccess/'); ?>" + role_id;
            }
        });

    });

    // Ajax tambah role
    $('#addNewRole').click(function() {
        $.post('http://localhost/PHP/CI-SistemLogin/admin/addRole', $('#tambahRoleModal').serialize(),
            function(data) {
                if (data.code === 1) {
                    $('#errorRole').html(data.role);
                } else {
                    location.href = 'http://localhost/PHP/CI-SistemLogin/admin/role';
                }
            }, 'json');
    });

    // Add Menu
    $('.addMenu').on('click', function() {
        $('.modal-title').html('Add New Sub Menu');
        $('#tombol').html('Add');

        $('#title').val('');
        $('#menu_id').val('').prop("disabled", false);
        $('#url').val('').prop("disabled", false);
        $('#icon').val('');

        $('.modal form').attr('action', 'http://localhost/PHP/CI-SistemLogin/menu/subMenu');
    })

    // edit submenu
    $('.editMenu').on('click', function() {
        const id = $(this).data('id');
        $('.modal-title').html('Edit Sub Menu');
        $('#tombol').html('Edit');
        $('.modal form').attr('action', 'http://localhost/PHP/CI-SistemLogin/menu/editSubmenu');

        $.ajax({
            url: 'http://localhost/PHP/CI-SistemLogin/menu/getUbah',
            data: {
                id: id
            },
            method: 'post',
            dataType: 'json',
            success: function(data) {
                $('#id').val(data.id);
                $('#title').val(data.title);
                $('#menu_id').val(data.menu_id).prop("disabled", true);
                $('#url').val(data.url).prop("disabled", true);
                $('#icon').val(data.icon);
            }
        });
    });
</script>

</body>

</html>