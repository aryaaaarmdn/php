<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {

        if ($this->session->userdata('email')) {
            redirect('user');
        }

        // rules
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Login Page';

            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else {
            $this->_login(); // menjalankan fungsi login
        }
    }

    private function _login()
    {
        // ambil email & password
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        // Query ke db
        $user = $this->db->get_where('user', ['email' => $email])->row_array();

        // cek
        if ($user) { // jika data user ada pada db
            // jika user active
            if ($user['is_active'] == 1) {
                // cek password
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'email' => $user['email'],
                        'role_id' => $user['role_id']
                    ];

                    $this->session->set_userdata($data);

                    if ($user['role_id'] == 1) {
                        redirect('admin');
                    } else {
                        redirect('user');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Wrong password
                    </div>');

                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Your Account not yet active
                </div>');

                redirect('auth');
            }
        } else { // jika data user tidak ada pada db
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Email is not register
            </div>');

            redirect('auth');
        }
    }

    public function registration()
    {

        if ($this->session->userdata('email')) {
            redirect('user');
        }

        //rules
        $this->form_validation->set_rules('name', 'Name', 'required|trim'); // trim = Menghapus spasi di awal dan akhir string
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'This email has been register'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[4]|matches[password2]', [
            'matches' => 'Password dont match!!',
            'min_length' => 'Password too short'
        ]);
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'required|trim|matches[password1]');

        // validasi
        // asumsi saya sendiri yaitu ketika tombol submit belum ditekan maka form validation belum berjalan
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Registration Page';

            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/registration');
            $this->load->view('templates/auth_footer');
        } else {

            // set timezone
            date_default_timezone_set("Asia/Jakarta");

            // preparing data what will be to db insert
            $email = $this->input->post('email', true);
            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($email),
                'image' => 'default.jpg',
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'is_active' => 0,
                'date_created' => date("Y-m-d H:i:s")
                // date("Y-m-d H:i:s") == date("Y-m-d H:i:s", time()) || parameter kedua yaitu time() berupa defaultnya
            ];

            // Siapkan token
            $token = base64_encode(random_bytes(32));
            $user_token = [
                'email' => $email,
                'token' => $token,
                'date_created' => time()
            ];

            // insert to db
            $this->db->insert('user', $data);
            $this->db->insert('user_token', $user_token);



            // kirim email
            $this->_sendEmail($data['email'], $token, 'verify');

            // flash message (pesan ketika sudah daftar)
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Congratulation! Your Account has been create. PLs Activate ur account
            </div>');

            // redirect to login page
            redirect('auth');
        }
    }

    private function _sendEmail($email, $token, $type)
    {
        $config = [
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_user' => 'aryaramadhan097@gmail.com',
            'smtp_pass' => 'mugenTsukuyomi_369',
            'smtp_port' => 465,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        ];

        $this->load->library('email', $config);
        $this->email->initialize($config);

        $this->email->from('aryaramadhan097@gmail.com', 'Admin');
        $this->email->to($email);

        if ($type == 'verify') {
            $this->email->subject('Account Verification');
            $this->email->message('Click This Link To Verify Your Account : <a href="' . base_url() . 'auth/verify?email=' . base64_encode(urlencode($email)) . '&token=' . urlencode($token) . '">Activate</a>');
        } else if ($type == 'forgot') {
            $this->email->subject('Reset Password');
            $this->email->message('Click This Link To Reset Your Password : <a href="' . base_url() . 'auth/resetpassword?email=' . base64_encode(urlencode($email)) . '&token=' . urlencode($token) . '">Reset Password</a>');
        }


        if ($this->email->send()) {
            return true;
        } else {
            echo $this->email->print_debugger();
            die;
        }
    }

    public function verify()
    {

        // ambil email & token dari url
        $email = $this->input->get('email');
        $email_decode = urldecode(base64_decode($email));

        $token = $this->input->get('token');

        $user = $this->db->get_where('user', ['email' => $email_decode])->row_array();

        // Jika ada user dengan email yg sudah di enkripsi
        if (!isset($user['email'])) { // jika email yang sudah di enkripsi diubah lalu menghasilkan hasil yang salah
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Failed Account Activation! Wrong Email
            </div>');

            // redirect to login page
            redirect('auth');
        } else {
            $user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();

            if ($user_token) {
                // waktu expired token
                // if (time() - $user_token['date_created'] < (60 * 60 * 24)) {
                if (time() - $user_token['date_created'] < 60) {
                    $this->db->set('is_active', 1);
                    $this->db->where('email', $email_decode);
                    $this->db->update('user');

                    $this->db->delete('user_token', ['email' => $email_decode]);
                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">' . $email_decode . ' Has Been Active, please login
                    </div>');

                    // redirect to login page
                    redirect('auth');
                } else {

                    $this->db->delete('user_token', ['email' => $email_decode]);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Activation Fail. Token is expired.<br> <a href="' . base_url('auth/resendtoken?email=') . '' . $email . '&type=verify">Click to Resend Token</a>
                    </div>');

                    // redirect to login page
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Activation Fail. Invalid Token
                </div>');

                // redirect to login page
                redirect('auth');
            }
        }
    }

    public function resendtoken()
    {
        $email = $this->input->get('email');
        $type = $this->input->get('type');
        $email_decode = urldecode(base64_decode($email));

        $token = base64_encode(random_bytes(32));
        $user_token = [
            'email' => $email_decode,
            'token' => $token,
            'date_created' => time()
        ];

        $this->db->insert('user_token', $user_token);

        // kirim email
        $this->_sendEmail($email_decode, $token, $type);

        // flash message (pesan ketika sudah daftar)
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Pls check ur email to see resend code!!
        </div>');

        // redirect to login page
        redirect('auth');
    }


    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        You have been logout
        </div>');

        // redirect to login page
        redirect('auth');
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }

    public function forgotPassword()
    {
        if ($this->session->userdata('email')) {
            redirect('user');
        }

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Forgot Password';

            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/forgot-password');
            $this->load->view('templates/auth_footer');
        } else {
            $email = $this->input->post('email');
            $user = $this->db->get_where('user', ['email' => $email, 'is_active' => 1])->row_array();

            if ($user) {
                $token = base64_encode(random_bytes(32));
                $user_token = [
                    'email' => $email,
                    'token' => $token,
                    'date_created' => time()
                ];

                $this->db->insert('user_token', $user_token);
                $this->_sendEmail($email, $token, 'forgot');

                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Pls Check ur email to reset ur password
                </div>');

                // redirect to login page
                redirect('auth/forgotPassword');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                This email not register or not yet to activate
                </div>');

                redirect('auth/forgotPassword');
            }
        }
    }

    public function resetpassword()
    {
        $email = $this->input->get('email');
        $email_decode = urldecode(base64_decode($email));

        $token = $this->input->get('token');
        $user = $this->db->get_where('user', ['email' => $email_decode])->row_array();

        if (!isset($user['email'])) {

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Reset Password Fail. Wrong Email!!
            </div>');

            // redirect to login page
            redirect('auth');
        } else {
            $user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();

            if ($user_token) {
                if (time() - $user_token['date_created'] < 60) {
                    $this->session->set_userdata('reset_email', $email_decode);
                    $this->db->delete('user_token', ['token' => $token]);
                    $this->changePassword();
                } else {
                    $this->db->delete('user_token', ['token' => $token]);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                            Reset Password Fail. Token is expired!! <a href="' . base_url('auth/resendtoken?email=') . '' . $email . '&type=forgot">Click to Resend Token</a></div>');

                    // redirect to login page
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                        Reset Password Fail. Invalid Token!!
                        </div>');

                // redirect to login page
                redirect('auth');
            }
        }
    }

    public function changePassword()
    {

        if (!$this->session->userdata('reset_email')) {
            redirect('auth');
        }

        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|matches[password2]');
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|min_length[4]|matches[password]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Change Password';

            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/change-password');
            $this->load->view('templates/auth_footer');
        } else {
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $email = $this->session->userdata('reset_email');

            $this->db->set('password', $password);
            $this->db->where('email', $email);
            $this->db->update('user');

            $this->session->unset_userdata('reset_email');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Password has been change, pls login
            </div>');

            redirect('auth');
        }
    }
}
