<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'Menu Management';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['menu'] = $this->db->get('user_menu')->result_array();


        $this->form_validation->set_rules('menu', 'Menu', 'required');


        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('user_menu', ['menu' => $this->input->post('menu', true)]);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    New Menu Added
                    </div>');

            redirect('menu');
        }
    }

    public function subMenu()
    {
        $data['title'] = 'Sub Menu Management';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['menu'] = $this->db->get('user_menu')->result_array();


        $this->load->model('Menu_model', 'menu');
        $data['subMenu'] = $this->menu->getSubMenu();

        $this->form_validation->set_rules('title', 'Menu', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data); // $data['title']
            $this->load->view('templates/sidebar', $data); // $data['menu'] (ketika add new submenu pada select box)
            $this->load->view('templates/topbar', $data); // $data['user']
            $this->load->view('menu/subMenu', $data); // $data['title'], $data['subMenu']
            $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title', true),
                'menu_id' => $this->input->post('menu_id', true),
                'url' => $this->input->post('url', true),
                'icon' => $this->input->post('icon', true),
                'is_active' => $this->input->post('is_active')
            ];

            $this->db->insert('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    New SubMenu Added
                    </div>');

            redirect('menu/subMenu');
        }
    }

    public function deleteMenu($menuID)
    {
        $this->db->delete('user_menu', ['id' => $menuID]);

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Success delete menu!
        </div>');

        redirect('menu');
    }

    public function deleteSubMenu($menuID)
    {
        $this->db->delete('user_sub_menu', ['id' => $menuID]);

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Success delete menu!
        </div>');

        redirect('menu/subMenu');
    }

    public function getUbah()
    {
        $getMenu = $this->db->get_where('user_sub_menu', ['id' => $_POST['id']])->row_array();
        echo json_encode($getMenu);
    }

    public function editSubMenu()
    {
        $id = $this->input->post('id');

        $data['title'] = 'Sub Menu Management';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->load->model('Menu_model', 'menu');
        $data['subMenu'] = $this->menu->getSubMenu();


        $this->form_validation->set_rules('title', 'Menu', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data); // $data['title']
            $this->load->view('templates/sidebar', $data); // $data['menu'] (ketika add new submenu pada select box)
            $this->load->view('templates/topbar', $data); // $data['user']
            $this->load->view('menu/subMenu', $data); // // $data['title'], $data['subMenu']
            $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title', true),
                'icon' => $this->input->post('icon', true)
            ];

            $this->db->where('id', $id);
            $this->db->update('user_sub_menu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    New SubMenu Changed
                    </div>');

            redirect('menu/subMenu');
        }
    }
}
