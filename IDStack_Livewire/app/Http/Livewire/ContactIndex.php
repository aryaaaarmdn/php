<?php

namespace App\Http\Livewire;

use App\Contact;
use Livewire\Component;
use Livewire\WithPagination;

class ContactIndex extends Component
{
    use WithPagination;

    // public $data; // cara pertama

    public $statusUpdate = false;
    public $confirmDelete;

    protected $listeners = [
        'contactStored' => 'handleStored',
        'contactUpdated' => 'handleUpdated'
    ];

    public function render()
    {
        // $this->data = Contact::latest()->get(); // cara pertama
        return view('livewire.contact-index', [
            'contacts' => Contact::latest()->paginate(5)
        ]);
    }

    public function getContact($id)
    {
        $this->statusUpdate = true;
        $contact = Contact::find($id);
        $this->emit('getContact', $contact);
    }

    public function confirmDelete($id)
    {
        $this->confirmDelete = $id;
    }

    public function deleteContact($id)
    {
        if ($id) {
            $data = Contact::find($id);
            $data->delete();

            session()->flash('message', 'Contact Deleted!!');
        }
    }

    public function handleStored($contact)
    {
        // dd($contact)
        session()->flash('message', 'Contact ' . $contact['name'] . ' Added!!');
    }

    public function handleUpdated($contact)
    {
        // dd($contact)
        session()->flash('message', 'Contact ' . $contact['name'] . ' Changed!!');
    }
}
