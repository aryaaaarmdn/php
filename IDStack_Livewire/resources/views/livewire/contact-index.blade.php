<div>
    
    @if (session()->has('message'))
        <div class="alert alert-success">{{ session('message') }}</div>
    @endif

    
    @if ($statusUpdate)
        <livewire:contact-update></livewire:contact-update>
    @else
        <livewire:contact-create></livewire:contact-create>
    @endif

    <hr>

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Phone</th>
                <th scope="col"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($contacts as $key => $contact)
            <tr>
                <th scope="row">{{ $contacts->firstItem() + $key }}</th>
                <td>{{ $contact->name }}</td>
                <td>{{ $contact->phone }}</td>
                <td>
                    <button wire:click="getContact({{ $contact->id }})" class="btn btn-info btn-sm text-white">Edit</button>
                    @if($confirmDelete === $contact->id)
                        <button wire:click="deleteContact({{ $contact->id }})"
                            class="btn btn-danger btn-sm text-white">Sure?</button>
                    @else
                        <button wire:click="confirmDelete({{ $contact->id }})"
                            class="btn btn-warning btn-sm text-dark">Delete</button>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $contacts->links() }}
</div>
